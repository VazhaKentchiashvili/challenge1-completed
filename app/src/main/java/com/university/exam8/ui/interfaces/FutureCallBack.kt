package com.university.exam8.ui.interfaces

import com.university.exam8.ui.dataLoader.DataLoader.RetrofitApi.Companion.API_ENDPOINT
import retrofit2.Response
import retrofit2.http.GET

interface FutureCallBack<T> {
    fun done(result: String) {}
    fun error(title: String, errorMessage: String) {}
}
